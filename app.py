from flask import Flask
import os 

app = flask(__name__)

@app.route("/")
def wish():
    message = "Happy Birthday {name}"
    return message.format(name=os.getenv("NAME","Baljeet"))

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8080)
