FROM python:3.8-slim-bullseye
WORKDIR /app
ADD . /app
RUN pip install --trusted-host pypi.python.org Flask
ENV NAME Preet
CMD ["python ", "app.py"]
